//Récupération des données du LocalStorage
let productsLocalStorageNoSorted = JSON.parse(localStorage.getItem("product"));
let productsCartSort;
let productsCartFinal = [];
let quantityTotalProducts;
let priceTotalProducts;
let inputsQuantity;
let orderInformation;
let products = [];
let contact;

//Tri des produits par nom, dans l'ordre alphabéthique
const sortProductsCart = () => {
  productsCartSort = productsLocalStorageNoSorted.sort((a, b) => {
    if (a.id < b.id) return -1;
    if (a.id > b.id) return 1;
    return 0;
  });
  // console.log(productsCartSort);
};

//Affichage du tableau sur la page panier
const displayProductsInCart = () => {
  console.log("début rechargement");
  document.getElementById("cart__items").innerHTML = productsCartSort
    .map(
      (product) => `
    <article class="cart__item" data-id="${product.id}">
    <div class="cart__item__img">
    <img src="${product.image}" alt="${product.altTxt}">
    </div>
    <div class="cart__item__content">
    <div class="cart__item__content__titlePrice">
    <h2>${product.name}</h2>
    <p><span class="itemPrice">${product.price * product.quantity}</span> €</p>
    </div>
    <div class="cart__item__content__settings">
    <div class="cart__item__content__settings__color">
    <p>Couleur : <span class="itemColor">${product.color}</span></p>
    </div>
    <div class="cart__item__content__settings__quantity">
    <p>Qté : </p>
    <input type="number" class="itemQuantity" name="itemQuantity" min="1" max="100" value="${
      product.quantity
    }">
    </div>
    <div class="cart__item__content__settings__delete">
    <p class="deleteItem">Supprimer</p>
    </div>
    </div>
    </div>
    </article>  
    `
    )
    .join("");

  inputsQuantity = document.querySelectorAll(".itemQuantity");
  setChangeQuantityEventsListeners();
  checkOrderForm();
  setProductCartDelete();
  console.log("fin rechargement");
};

const setTotalQuantity = () => {
  let quantities = document.querySelectorAll(".itemQuantity");
  let quantityEachProduct = [];
  for (let i = 0; i < quantities.length; i++) {
    quantityEachProduct.push(parseInt(quantities[i].value));
  }
  // console.log(quantityEachProduct);

  quantityTotalProducts = quantityEachProduct.reduce((acc, x) => {
    return acc + x;
  });

  // console.log(quantityTotalProducts);
};

const setTotalPrice = () => {
  let prices = document.querySelectorAll(".itemPrice");
  let priceEachProduct = [];
  // console.log(prices);
  for (let i = 0; i < prices.length; i++) {
    priceEachProduct.push(parseInt(prices[i].innerText));
  }
  // console.log(priceEachProduct);

  priceTotalProducts = priceEachProduct.reduce((acc, x) => {
    return acc + x;
  });

  // console.log(priceTotalProducts);
};

const displayTotals = () => {
  setTotalQuantity();
  setTotalPrice();

  document.getElementById("totalQuantity").textContent = quantityTotalProducts;
  document.getElementById("totalPrice").textContent = priceTotalProducts;
};

//Changement de quantité d'un produit

const setChangeQuantityEventsListeners = () => {
  // console.log(inputsQuantity);
  inputsQuantity.forEach((element) => {
    // console.log("bonjour");
    element.addEventListener("input", (e) => {
      e.preventDefault();
      let classELementParent = e.target.parentElement.className;

      if (classELementParent == "cart__item__content__settings__quantity") {
        let titleParentProduct =
          e.target.parentElement.parentElement.parentElement.firstChild
            .nextSibling.firstElementChild.textContent;
        let colorParentProduct =
          e.target.parentElement.parentElement.querySelector(
            ".itemColor"
          ).textContent;
        let newQuantity = e.target.value;
        let done = false;

        // console.log(colorParentProduct);
        // console.log(classELementParent);
        // console.log(titleParentProduct);
        // console.log(newQuantity);

        for (let i = 0; i < productsLocalStorageNoSorted.length; i++) {
          const elt = productsLocalStorageNoSorted[i];
          if (
            titleParentProduct == elt.name &&
            colorParentProduct == elt.color
          ) {
            elt.quantity = newQuantity;
            done = true;
            break;
          }
        }
        // console.log(productsLocalStorageNoSorted);
        if (done == true) {
          localStorage.setItem(
            "product",
            JSON.stringify(productsLocalStorageNoSorted)
          );
          displayProductsInCart();
          setTotalPrice();
          setTotalQuantity();
          displayTotals();
        }
      }
    });
  });
};

//Vérification formulaire
const checkOrderForm = () => {
  let firstName = document.getElementById("firstName");
  let lastName = document.getElementById("lastName");
  let address = document.getElementById("address");
  let city = document.getElementById("city");
  let email = document.getElementById("email");

  let buttonOrder = document.getElementById("order");

  buttonOrder.disabled = true;

  //Vérification format prénom
  firstName.addEventListener("input", (e) => {
    let regex = /^[a-zA-Z\-]+$/;
    if (!regex.test(e.target.value)) {
      if (e.target.value == "") {
        document.getElementById("firstNameErrorMsg").textContent =
          "Veuillez renseigner ce champ";
        buttonOrder.disabled = true;
      } else {
        document.getElementById("firstNameErrorMsg").textContent =
          "Les chiffres ne sont pas autorisés";
        buttonOrder.disabled = true;
      }
    } else {
      document.getElementById("firstNameErrorMsg").textContent = "";
      buttonOrder.disabled = false;
    }
  });

  //Vérification format nom
  lastName.addEventListener("input", (e) => {
    let regex = /^[a-zA-Z\-]+$/;
    if (!regex.test(e.target.value)) {
      if (e.target.value == "") {
        document.getElementById("lastNameErrorMsg").textContent =
          "Veuillez renseigner ce champ";
        buttonOrder.disabled = true;
      } else {
        document.getElementById("lastNameErrorMsg").textContent =
          "Les chiffres ne sont pas autorisés";
        buttonOrder.disabled = true;
      }
    } else {
      document.getElementById("lastNameErrorMsg").textContent = "";
      buttonOrder.disabled = false;
    }
  });

  //Vérification format adresse
  address.addEventListener("input", (e) => {
    let regex = /^[a-zA-Z\-0-9]+$/;
    if (!regex.test(e.target.value)) {
      if (e.target.value == "") {
        document.getElementById("addressErrorMsg").textContent =
          "Veuillez renseigner ce champ";
        buttonOrder.disabled = true;
      } else {
        document.getElementById("addressErrorMsg").textContent = "";
        buttonOrder.disabled = true;
      }
    } else {
      document.getElementById("addressErrorMsg").textContent = "";
      buttonOrder.disabled = false;
    }
  });

  //Vérification format ville
  city.addEventListener("input", (e) => {
    let regex = /^[a-zA-Z\-]+$/;
    if (!regex.test(e.target.value)) {
      if (e.target.value == "") {
        document.getElementById("cityErrorMsg").textContent =
          "Veuillez renseigner ce champ";
        buttonOrder.disabled = true;
      } else {
        document.getElementById("cityErrorMsg").textContent =
          "Les chiffres ne sont pas autorisés";
        buttonOrder.disabled = true;
      }
    } else {
      document.getElementById("cityErrorMsg").textContent = "";
      buttonOrder.disabled = false;
    }
  });

  //Vérification format email
  email.addEventListener("input", (e) => {
    let regex = /^[\w\-\+]+(\.[\w\-]+)*@[\w\-]+(\.[\w\-]+)*\.[\w\-]{2,4}$/;
    if (!regex.test(e.target.value)) {
      if (e.target.value == "") {
        document.getElementById("emailErrorMsg").textContent =
          "Veuillez renseigner ce champ";
        buttonOrder.disabled = true;
      } else {
        document.getElementById("emailErrorMsg").textContent =
          "Le mail n'est pas correct";
        buttonOrder.disabled = true;
      }
    } else {
      document.getElementById("emailErrorMsg").textContent = "";
      buttonOrder.disabled = false;
    }
  });
};

//Suppression article
const setProductCartDelete = () => {
  buttonsDelete = document.querySelectorAll(".deleteItem");
  console.log(buttonsDelete);
  buttonsDelete.forEach((element) => {
    element.addEventListener("click", (e) => {
      console.log(e);
      nameProduct =
        e.target.parentElement.parentElement.parentElement.firstElementChild
          .firstElementChild.textContent;
      colorProduct =
        e.target.parentElement.parentElement.parentElement.querySelector(
          ".itemColor"
        ).textContent;
      console.log(nameProduct);
      console.log(colorProduct);
      for (let i = 0; i < productsLocalStorageNoSorted.length; i++) {
        if (
          nameProduct === productsLocalStorageNoSorted[i].name &&
          colorProduct === productsLocalStorageNoSorted[i].color
        ) {
          console.log(
            "le produit " +
              productsLocalStorageNoSorted[i].name +
              " est supprimé"
          );
          productsLocalStorageNoSorted.splice(i, 1);
          localStorage.setItem(
            "product",
            JSON.stringify(productsLocalStorageNoSorted)
          );
          displayProductsInCart();
          setTotalPrice();
          setTotalQuantity();
          displayTotals();
          break;
        }
      }
    });
  });
};

//Récupérations des données de la page panier
const setOrderInformations = () => {
  if (products == "") {
    productsCartSort.map((product) => {
      products.push(product.id);
    });
  }

  contact = {
    firstName: document.getElementById("firstName").value.toLowerCase(),
    lastName: document.getElementById("lastName").value.toLowerCase(),
    address: document.getElementById("address").value.toLowerCase(),
    city: document.getElementById("city").value.toLowerCase(),
    email: document.getElementById("email").value.toLowerCase(),
  };

  console.log(contact);
  console.log(products);
};

//Envoi des données à la base de donnée "order"
const setPostOrderInformations = async () => {
  await setOrderInformations();

  data = {
    contact: contact,
    products: products,
  };

  console.log(data);

  fetch("http://localhost:3000/api/products/order", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json" },
    mode: "cors",
    credentials: "same-origin",
  })
    .then((response) => response.json())
    .then((json) => {
      localStorage.setItem("orderId", json.orderId);
      window.location.href = "confirmation.html";
    });
};
//Affichage "order_id" sur la page confirmation

let submit = document.getElementById("order");
submit.addEventListener("click", (e) => {
  e.preventDefault();
  setPostOrderInformations();
});

//appel des fonctions
sortProductsCart();
displayProductsInCart();
displayTotals();
