// Initialisation variables
let productsData = [];

// fonctions

// Récupérations des données dans la DB
const getProducts = async () => {
  await fetch("http://localhost:3000/api/products")
    .then((res) => res.json())
    .then((data) => (productsData = data));
};

// Affichage des produits sur la page
const displayProducts = async () => {
  await getProducts();

  document.getElementById("items").innerHTML += productsData
    .map(
      (product) => `
    <a href="./product.html?id=${product._id}">
    <article>
      <img src="${product.imageUrl}" alt="${product.altTxt} ">
      <h3 class="productName">${product.name}</h3>
      <p class="productDescription">${product.description}</p>
    </article>
     </a>   
    `
    )
    .join("");
};

//script

displayProducts();
