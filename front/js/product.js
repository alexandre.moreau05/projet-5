// Recherche id dans l'url : urlSearchParams

// Initialisation variable
let productId = new URLSearchParams(document.location.search.substring(1)).get(
  "id"
);
let productData = [];
let productDataForLS = [];

// Récupération des données du produit
const getProducData = async () => {
  await fetch(`http://localhost:3000/api/products/${productId}`)
    .then((res) => res.json())
    .then((data) => (productData = data));

  // console.log(productData);
};

//Affichage des données du produit
const displayProductData = async () => {
  await getProducData();

  //récupération des élements HTML
  const eltImage = document.getElementById("product__img");
  const eltTitle = document.getElementById("title");
  const eltPrice = document.getElementById("price");
  const eltDescription = document.getElementById("description");
  const eltChoiceColors = document.getElementById("colors");
  //récupération tableau des couleurs
  const productColors = productData.colors;

  //affichage image
  eltImage.setAttribute("src", productData.imageUrl);
  eltImage.setAttribute("alt", productData.altTxt);
  //affichage nom
  eltTitle.textContent = productData.name;
  //affichage prix
  eltPrice.textContent = productData.price;
  //affichage description
  eltDescription.textContent = productData.description;

  //option de couleur
  // console.log(productColors);
  eltChoiceColors.innerHTML += productColors.map(
    (colors) => `
      <option value="${colors}">${colors}</option>
      `
  );
};

//récupération données du produit dans un objet
const formatProductDataForLS = async () => {
  await getProducData();

  productDataForLS = {
    id: productId,
    name: productData.name,
    price: productData.price,
    color: document.getElementById("colors").value,
    quantity: parseInt(document.getElementById("quantity").value),
    image: productData.imageUrl,
    altTxt: productData.altTxt,
  };

  console.log(productDataForLS);
};

const addProductToCart = async () => {
  await formatProductDataForLS();

  let productsInLocalStorage = JSON.parse(localStorage.getItem("product"));
  console.log(productsInLocalStorage);

  if (productsInLocalStorage != null) {
    // console.log(productsInLocalStorage);
    let done = false;
    for (let i = 0; i < productsInLocalStorage.length; i++) {
      let currentProductFromLS = productsInLocalStorage[i];
      const product = productDataForLS;

      if (
        product.id == currentProductFromLS.id &&
        product.color == currentProductFromLS.color
      ) {
        currentProductFromLS.quantity += product.quantity;
        done = true;
        break;
      }
    }
    if (!done) {
      productsInLocalStorage.push(productDataForLS);
      // localStorage.setItem("product", JSON.stringify(productsInLocalStorage));
    }
  } else {
    productsInLocalStorage = [];
    productsInLocalStorage.push(productDataForLS);
  }
  alert("Produit ajouter au panier");
  localStorage.setItem("product", JSON.stringify(productsInLocalStorage));
};

//Envoi des données au local storage au clique sur le bouton "Ajouter au panier"
document
  .getElementById("addToCart")
  .addEventListener("click", addProductToCart);

// script
displayProductData();
